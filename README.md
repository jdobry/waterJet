# waterJet
RC model water jet made in OpenSCAD

Project homepage https://www.thingiverse.com/thing:2872620


To compile this project I am using  openscad-2018.04.06 (devel version)

+ these libraries

	https://github.com/openscad/scad-utils

	https://github.com/openscad/list-comprehension-demos
